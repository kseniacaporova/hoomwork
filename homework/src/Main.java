import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Введите имя: ");
        String name = sc.next();

        System.out.print("Введите возраст: ");
        int age = sc.nextInt();

        System.out.print("Введите сумму запрашиваемого кредита: ");
        int sum = sc.nextInt();

        if (!name.equals("Bob") && age >= 18 &&  sum <= age * 100) {
            System.out.println( name+ ", кредит одобрен");
        }else{
            System.out.println(name + ", кредит не одобрен");
        }
    }
}
